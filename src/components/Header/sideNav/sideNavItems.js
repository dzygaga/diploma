import React from 'react';
import { Link } from "react-router-dom";
import FontAwesome from "react-fontawesome";
import style from "./style.css";



class SideNavItems extends React.Component {
     items = [
        {
            type: style.option,
            icon: 'home',
            text: 'Home',
            link: '/'
        },
        {
            type: style.option,
            icon: 'file-text-o',
            text: 'News',
            link: '/articles'
        },
        {
            type: style.option,
            icon: 'play',
            text: 'Videos',
            link: '/videos'
        },
        {
            type: style.option,
            icon: 'sign-in',
            text: 'Sign-in',
            link: '/sign-in'
        },
        {
            type: style.option,
            icon: 'sign-out',
            text: 'Sign-out',
            link: '/sign-out'
        },
    ]

    showItems = () => {
        return this.items.map( (item,i) =>{
            return (
                <div key={i} className={item.type}>
                    <Link to={item.link}>
                        <FontAwesome name={item.icon}/>
                        {item.text}
                    </Link>
                </div>
            )
        } )
    }


    render() {

        return (
            <div>
                {this.showItems()}
            </div>
        )
    }
}

export default SideNavItems