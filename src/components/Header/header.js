import React from 'react';
import style from './header.css';
import {NavLink} from "react-router-dom";
import SideNavigatoin from "./sideNav/sideNav";
import FontAwesome from "react-fontawesome";


class Header extends React.Component {

    navBars = () => (
        <div className={style.bars}>
            <FontAwesome name="bars"
                         onClick={this.props.onOpenNav}
                         style={{
                             color: '#dfdfdf',
                             padding: '10px',
                             cursor: 'pointer'
                         }}
            />
        </div>
    )

    render() {

        return (
            <header className={style.header}>

                <div className="fixed-top">
                    <header className="topbar">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <ul className="social-network">
                                        <li><NavLink className="waves-effect waves-dark"
                                                     to="https://uk-ua.facebook.com/"><i className="fa fa-facebook"></i></NavLink>
                                        </li>
                                        <li><NavLink className="waves-effect waves-dark" to="https://twitter.com/"><i
                                            className="fa fa-twitter"></i></NavLink></li>
                                        <li><NavLink className="waves-effect waves-dark" to="https://www.linkedin.com/"><i
                                            className="fa fa-linkedin"></i></NavLink></li>
                                        <li><NavLink className="waves-effect waves-dark"
                                                     to="https://www.pinterest.com/"><i className="fa fa-pinterest"></i></NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </header>
                    <nav className="navbar navbar-expand-lg navbar-dark mx-background-top-linear">
                        <div className="container">
                            <NavLink className="navbar-brand" to="https://lacodeid.com/">SPORTNEWSNBA.COM</NavLink>
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item active"><NavLink className="nav-link" to="/">Home<span
                                    className="sr-only">(current)</span></NavLink></li>
                            </ul>
                            <div className="burger">
                                <SideNavigatoin {...this.props} />
                                <div className={style.headerOpt}>
                                    {this.navBars()}
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div className="margin-blank"></div>
            </header>
        )
    }
}
export default Header