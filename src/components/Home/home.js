import React from 'react';
import HeaderSlider from '../vidjed/headerSlider/headerSlider';
import VideosList from '../vidjed/videos/videosList';
import NewsList from '../vidjed/newsList/newsList';



class Home extends React.Component {

    render() {

        return (
            <div>
                <HeaderSlider
                    type="featured"
                    start={0}
                    amount={3}
                    settings={{
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        speed: 600
                    }}
                />
                <NewsList
                    start={0}
                    amount={6}
                    type="card"
                    loadmore={true}
                />
                <VideosList
                    type="card"
                    title={true}
                    loadmore={true}
                    start={0}
                    amount={3}
                />
            </div>


        )
    }
}
export default Home