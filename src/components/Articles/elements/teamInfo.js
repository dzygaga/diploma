import React from 'react';
import  './teamInfo.css';

class TeamInfo extends React.Component {

    render() {

        return (
            <div className="teamInfo">
                <div className="teamInfo_logo"
                    style={{
                        background: `url('/images/teams/${this.props.team.logo}')`,
                        }}
                >
                </div>
                <div className="teamInfo_team">
                    {this.props.team.city}
                    {this.props.team.name}
                </div>
                <div className="teamInfo_games">
                    W: {this.props.team.stats[0].wins};
                    L: {this.props.team.stats[0].defeats}
                </div>
            </div>


        )
    }
}
export default TeamInfo