import React from 'react';
import './postdata.css';

class PostDate extends React.Component {

    render() {

        return (
            <div className="DateAuthor">
                <div className="postDate">Date:
                    {this.props.data.date}
                </div>
                <div className="postAuthor">Author:
                    {this.props.data.author}
                </div>
            </div> 
        )
    }
}
export default PostDate