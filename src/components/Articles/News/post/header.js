import React from 'react';
import TeamInfo from '../../elements/teamInfo';
import PostDate from '../../elements/postdate';

class Header extends React.Component {

    teamInfo = (team) => {
        return team ? (
            <TeamInfo team={team} />
        ) : null;
    }

    postData = (date, author) => {
        return date, author?(
        <PostDate data={{
            date, author
        }} />
            ): null;
    }

    render() {

        return (
            <div>
                {this.teamInfo(this.props.teamData)}
                {this.postData(this.props.date, this.props.author)}
            </div>
        )
    }
}
export default Header