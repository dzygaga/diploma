import React from 'react';
import {Link} from 'react-router-dom';
import CardInfo from '../cardInfo/cardInfo';




class VideosItem extends React.Component {

    render() {

        return this.props.data.map( (item,i) => (
            <Link to={`/videos/${item.id}`} key={i}>
                <div className="videoListItem_wrapper">
                    <div className="left"
                        style={{
                            background:`url(/images/videos/${item.image})`,
                            padding: '20px',
                            margin: '5px',
                        }}
                    >
                        <div></div>
                    </div>
                    <div className="right">
                        <CardInfo teams={this.props.teams} team={item.team} date={item.date}/> 
                        <h4>{item.title}</h4>
                    </div>
                </div>
            </Link>
        ))
    }
}
export default VideosItem