import React from 'react';
import axios from 'axios';
import VideosItem from '../videos/videosItem';
import Button from '../buttons/buttons';
import { URL } from '../configUrl';



class VideosList extends React.Component {
    state = {
        teams:[],
        videos:[],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    componentWillMount(){
        this.request(this.state.start, this.state.end)
    }

    request = (start,end) => {
        if(this.state.teams.length < 1){
            axios.get(`${URL}/teams`)
            .then( response => {
                this.setState({
                    teams:response.data
                })
            })
        }

        axios.get(`${URL}/videos?_start=${start}&_end=${end}`)
        .then( response => {
            this.setState({
                videos:[...this.state.videos,...response.data],
                start,
                end
            })
        })
    }

    renderVideos = () => {
        let template = null;

        switch(this.props.type){
            case('card'):
                template = <VideosItem data={this.state.videos} teams={this.state.teams}/>
                break;
            default:
                template = null
        }
        return template;
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount;
        this.request(this.state.end, end)
    }


    renderTitle = () => {
        return this.props.title ?
            <h1><strong> NBA VIDEOS </strong> </h1>
            : null
    }

    render(){
        return(
            <div className="videoList_wrapper">
                { this.renderTitle() }
                { this.renderVideos()}
                <Button
                    type="loadmore"
                    loadMore={()=>this.loadMore()}
                    cta="LOAD MORE VIDEOS"
                />
            </div>
        )
    }
}
export default VideosList


