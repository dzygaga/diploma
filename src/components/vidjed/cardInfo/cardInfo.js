import React from 'react';
import FontAwesome from 'react-fontawesome';





class CardInfo extends React.Component {
    teamName = (teams, team) => {
        let data = teams.find((item) => {
            return item.id === team;
        })
        if (data){
            return data.name;
        }
    }

    

    render() {
        return (
            <div className="cardNfo">
            <span className="teamName">
                {this.teamName(this.props.teams,this.props.team)}
            </span>
            <span className="date">
                <FontAwesome name="clock-o"/>
                {this.props.date}
            </span>
        </div>
        )
    }
}

export default CardInfo