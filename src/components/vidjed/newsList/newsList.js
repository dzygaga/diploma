import React from 'react';
import axios from 'axios';
import CardInfo from "../cardInfo/cardInfo";
import { URL } from '../configUrl';
import { Link } from 'react-router-dom';
import styles from './newsList.css';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import Button from '../buttons/buttons';





class NewsList extends React.Component {

    state = {
        teams: [],
        items: [],
        start: this.props.start,
        end: this.props.start + this.props.amount,
        amount: this.props.amount
    }

    request = (start, end) => {
        if (this.state.teams < 1) {
            axios.get(`${URL}/teams`)
                .then(response => {
                    this.setState({
                        teams: response.data
                    })
                })
        }

        axios.get(`${URL}/articles?=${start}&_end=${end}`)
            .then(response => {
                this.setState({
                    items: [...this.state.items, ...response.data],
                    start,
                    end
                })
            })

    }
    componentWillMount() {
        this.request(this.state.start, this.state.end);
    }


    renderNews = (type) => {
        let template = null;

        switch(type){
            case('card'):
                template = this.state.items.map((item,i) => (
                    <CSSTransition
                        classNames={{
                            enter:styles.newsList_wrapper,
                            enterActive:styles.newsList_wrapper_enter
                        }}
                        timeout={500}
                        key={i}
                    >
                        <div>
                            <div className="newslist_item">
                                <Link to={`/articles/${item.id}`}>
                                    <CardInfo teams={this.state.teams} team={item.team} date={item.date}/>
                                    <h2>{item.title}</h2>
                                </Link>
                            </div>
                        </div>
                    </CSSTransition>
                    
                ))
                break;
            case('cardMain'):
                template = this.state.items.map((item,i) => (
                    <CSSTransition
                    classNames={{
                        enter:styles.newsList_wrapper,
                        enterActive:styles.newsList_wrapper_enter
                    }}
                    timeout={500}
                    key={i}
                    >
                        <Link to={`/articles/${item.id}`}>
                            <div className="flex_wrapper">
                                <div className="left"
                                    style={{
                                        background:`url('/images/articles/${item.image}')`
                                    }}>
                                    <div></div>
                                </div>
                                <div className="right">
                                    <CardInfo teams={this.state.teams} team={item.team} date={item.date}/>
                                    <h2>{item.title}</h2>
                                </div>
                            </div>
                        </Link>
                    </CSSTransition>
                ))
                break;
            default:
                template = null;
        }

        return template;
    }

    loadMore = () => {
        let end = this.state.end + this.state.amount;
        this.request(this.state.end,end)
    }



    render(){
        console.log(this.state.items)
        
        return(
            <div className="newsList">
                <h1><strong>NBA NEWS </strong></h1>
                <TransitionGroup
                    component="div"
                    className="list"
                >
                    {this.renderNews(this.props.type)}
                </TransitionGroup>
                <Button
                    type="loadmore"
                    loadMore={()=>this.loadMore()}
                    cta="LOAD MORE NEWS"
                />
            </div>
        )
    }
}
export default NewsList