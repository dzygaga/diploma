import React from 'react';
import Header from '../Header/header';
import Footer from '../Footer/footer';

class Layout extends React.Component {

    state = {
        showNav: false
    }

    toggleSidenav = (action) =>{
        this.setState({
            showNav:action
        })
    }


    render() {


        return (
            <div>
                <Header 
                showNav={this.state.showNav}
                onHideNav={() => this.toggleSidenav(false)}
                onOpenNav={() => this.toggleSidenav(true)}

                />
                {this.props.children}
               <Footer />
            </div>
        )
    }
}
export default Layout