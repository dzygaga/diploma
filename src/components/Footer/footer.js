import React from 'react';
import  './footer.css';
import { NavLink } from "react-router-dom";


class Footer extends React.Component {

    render() {

        return (
            <div>

                <footer className="py-5 bg-dark">
                    <div id="copyright">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4">
                                    <p className="pull-left"><NavLink to="https://lacodeid.com/">© 2018 YOUR SPORTNEWSNBA.COM</NavLink></p>
                                </div>
                                <div className="col-md-8">
                                    <ul className="list-inline navbar-right">
                                        <li><NavLink className="a-font" to="/">Home</NavLink></li>
                                        <li><NavLink className="a-font" to="/articles">News</NavLink></li>
                                        <li><NavLink className="a-font" to="/videos">Videos</NavLink></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}
export default Footer