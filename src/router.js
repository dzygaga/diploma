import React from 'react';
import Layout from "./components/BaseLayout/layout";
import { Route, Switch } from "react-router-dom";
import Home from './components/Home/home';
import NewsArticles from './components/Articles/News/post/post';
import Videos from './components/Articles/Videos/Video/index';
import VideosList from './components/Videos/videos';
import News from "./components/News/news";

class Routes extends React.Component {

    render() {

        return (
            <Layout>
                <Switch>
                    <Route path="/articles/:id" exact component={NewsArticles} />
                    <Route path="/videos/:id" exact component={Videos} />
                    <Route path="/videos/" exact component={VideosList} />
                    <Route path="/articles/" exact component={News} />
                    <Route path="/" exact component={Home} />
                    <Route render={() =>
                            <h3>Oops...404...</h3>
                        } />
                </Switch>
            </Layout>
        )
    }
}
export default Routes;